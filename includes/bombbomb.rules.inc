<?php

function bombbomb_rules_action_info() {
  $items['bombbomb_add_contact'] = array(
    'label' => t('Add a contact to a BombBomb list'),
    'group' => 'BombBomb',
    'parameter' => array(
      'entity' => array(
        'type' => 'entity',
        'label' => t('Entity'),
        'allow null' => FALSE,
      ),
    ),
  );

  return $items;
}
