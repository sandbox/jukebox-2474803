<?php

/**
 * @file
 * BombBomb module admin settings.
 */

function bombbomb_admin_settings() {
  $bombbomb = bombbomb_get_instance();

  $form['bombbomb_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('BombBomb API Key'),
    '#required' => TRUE,
    '#default_value' => variable_get('bombbomb_api_key', ''),
    '#description' => t('Enter the API Key as listed under the Integrations tab in your account at bombbomb.com.'),
  );

  if(!empty($bombbomb->api_key)) {
    $form['bombbomb_mailling_list_id'] = array(
      '#type' => 'select',
      '#options' => bombbomb_get_lists(),
      '#title' => 'Subscription List',
      '#required' => TRUE,
      '#default_value' => variable_get('bombbomb_mailling_list_id', ''),
      '#description' => t('Select the mailing list that new users should be subscribed to.'),
    );
  }

  return system_settings_form($form);
}
