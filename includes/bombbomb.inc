<?php

/**
 * @file
 * Wrapper class around the BombBomb API.
 */

class BombBombException extends Exception {
}

/**
 * Class BombBomb
 */
class BombBomb {
  /**
   * The BombBomb supplied API Key
   * @var  string
   */
  public $api_key;

  /**
   * The BombBomb endpoint
   * @var  string
   */
  public $endpoint;

  public function __construct() {
    $api_key = variable_get('bombbomb_api_key', '');
    if (empty($api_key)) {
      throw new BombBombException('You must add a BombBomb API Key before you can use the BombBomb service.');
    }

    $this->api_key = $api_key;
    $this->endpoint = 'https://app.bombbomb.com/app/api/api.php';
  }

  public function sendRequest($params) {
    $options = array();

    $options['method'] = 'POST';
    $options['headers'] = array(
      'Content-type' => 'application/x-www-form-urlencoded',
    );

    $data = array(
      'api_key' => $this->api_key,
    );

    foreach ($params as $key => $param) {
      $data[$key] = $param;
    }

    $options['data'] = drupal_http_build_query($data);
    $response = drupal_http_request($this->endpoint, $options)->data;
    return json_decode($response);
  }

  public function GetLists() {
    $params = array(
      'method' => 'GetLists',
    );

    return $this->sendRequest($params);
  }

  /**
   * @param array $values
   * An array of customer information to add to the BombBomb list
   */
  public function AddContact($values = array()) {
    $params = array(
      'method' => 'AddContact',
      'eml' => isset($values['email']) ? $values['email'] : '',
      'firstname' => isset($values['first_name']) ? $values['first_name'] : '',
      'lastname' => isset($values['last_name']) ? $values['last_name'] : '',
      'listlist' => isset($values['list']) ? $values['list'] : '',
    );

    return $this->sendRequest($params);
  }
}
