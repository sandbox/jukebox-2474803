<?php

/**
 * @file
 * Checkout pane callback functions.
 */

/**
 * Implementation of hook_settings_form().
 */
function commerce_bombbomb_pane_settings_form($checkout_pane) {
  $form = array();

  $form['commerce_bombbomb_checkout_pane_mailling_list'] = array(
    '#type' => 'select',
    '#title' => 'Choose the mailing list to subscribe customers to.',
    '#options' => bombbomb_get_lists(),
    '#default_value' => variable_get('commerce_bombbomb_checkout_pane_mailling_list', ''),
  );

  return $form;
}

/**
 * Implementation of hook_form().
 */
function commerce_bombbomb_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  $pane = array();

  $pane['commerce_bombbomb_checkout_pane_mailing_list_checkbox'] = array(
    '#type' => 'checkbox',
    '#title' => 'Subscribe to the mailing list',
    '#default_value' => variable_get('commerce_bombbomb_checkout_pane_mailing_list_checkbox', '1'),
  );

  return $pane;
}

function commerce_bombbomb_pane_checkout_form_submit($form, &$form_state, $checkout_pane, $order) {
  if($form_state['values']['commerce_bombbomb']['commerce_bombbomb_checkout_pane_mailing_list_checkbox'] == 1) {
    $order->data[$checkout_pane['pane_id']] = $form_state['values']['commerce_bombbomb']['commerce_bombbomb_checkout_pane_mailing_list_checkbox'];
  }
}
